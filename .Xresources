! FONT RENDERING
! -----------------------------------------------------------------------------
  Xft.antialias:  true
  Xft.hinting:    false
  Xft.hintstyle:  0
  Xft.dpi:        102
  Xft.rgba:       none


! XTERM
! -----------------------------------------------------------------------------
  xterm*faceName:           "DejaVu Sans Mono"
  xterm*faceSize:           10
  xterm*scaleHeight:        0.95
  xterm*allowBoldFonts:     true
  xterm*allowWindowOps:     true
  xterm*boldMode:           false
  xterm*charClass:          33:48,35:48,37:48,43:48,45-47:48,64:48,95:48,126:48,35:48,58:48,63:48,61:48,44:48,38:48,59:48
  xterm*cursorBlink:        false
  xterm*cutNewline:         true

  xterm*scrollKey:          true
  xterm*scrollTtyOutput:    false
  xterm*fastScroll:         true
  xterm*jumpScroll:         true  
  xterm*saveLines:          1024000

  xterm*fullscreen:         false
  xterm*iconHint:           /home/vermaden/.icons/vermaden/xterm.xpm
  xterm*internalBorder:     1

  xterm*keepSelection:      true
  xterm*loginShell:         true
  xterm*metaSendsEscape:    true
  xterm*multiScroll:        true
  xterm*omitTranslation:    fullscreen
  
  xterm*on4Clicks:          group
  xterm*on5Clicks:          page
  xterm*selectToClipboard:  true

  xterm*SimpleMenu*font:    -*-clean-*-*-*-*-*-*-*-*-*-*-iso8859-2
  xterm*termName:           xterm-256color
  xterm*title:              xterm
  xterm*veryBoldColors:     14


  xmessage*font:            -*-inconsolata-medium-r-normal-*-14-*-*-*-*-*-iso8859-1
  xmessage*background:      #2E3440
  xmessage*foreground:      #D8DEE9
  xmessage*Scrollbar.width: 4
  xmessage*Scrollbar.borderWidth: 0
  xmessage*Buttons:         Close
  xmessage*defaultButton:   Close
  xmessage*form.Close.shapeStyle: rectangle
  xmessage*form.Close.borderWidth: 1

  xfontsel*font:            -*-dejavu sans mono-medium-r-normal-*-17-120-100-100-*-*-iso8859-1
  xfontsel*background:      #000000
  xfontsel*foreground:      #cccccc

  xterm*VT100*geometry:     150x40
  xterm.vt100.translations: #override \n\
    <Btn1Up>: select-end(PRIMARY, CLIPBOARD, CUT_BUFFER0) \n\
    Ctrl Shift <Key> V: insert-selection(CLIPBOARD) \n\
    Ctrl Shift <Key> C: copy-selection(CLIPBOARD) \n\
    Ctrl <Key> minus: smaller-vt-font() \n\
    Ctrl <Key> plus: larger-vt-font() \n\
    Ctrl <Key> 0: set-vt-font(d)


! CONFIGURING XTERM VIA XRESOURCES
! -----------------------------------------------------------------------------
!
! Originaly xterm was configured with the file ~/.Xdefaults
! Today xterm is more commonly configured with the file ~/.Xresources
!
! Typically when setting up a machine with a window manager, the file ~/.xinitrc is used to start up the X Window system
! And so what we will be doing is getting our ~/.xinitrc to load our ~/.Xresources file
!
! Whilst the ~/.Xresources file can be used as a single to both configure and theme xterm and other X applications, we will not do this.
! What we want to do instead is try to separate the configuration aspects from the appearance by using multiple files.
! And so we have created a directory structure like so: ~/.config/Xresources/themes/
! Inside this directory structure we have placed some themes.
! One theme that is very nice is the dark Nord theme.
! It so happens that they have created a theme for .Xresources which we have downloaded from their website.
!
! Here is where we have placed the file for our dark Nord theme:
! ~/.config/Xresources/themes/dark.nord
!
! And so, to load our configuration file, we need to add this line to ~/.xinitrc
! xrdb -load $HOME/.Xresources
!
! And finally, to load our theme file, we also need to add this line to ~/.xinitrc
! xrdb -merge $HOME/.config/Xresources/themes/dark.nord
!
! What xrdb does is manage any files we wish to use to configure .Xresources
! We have the -load functionality to load any file we wish to use for configuring .Xresources.
! Indeed if we wished to use .Xdefaults instead of .Xresources, then we would use xrdb like so:
! xrdb -load $HOME/.defaults
! But as we said earlier, today the .Xresources file is more commonly used.
!
! xrdb also gives us the -merge functionality to combine files.
! This is what we are using to our advantage here.
! By specifying a different file in the xrdb -merge "path to theme" command we can easily swap themes!
!
! Note that we have used the $HOME environmental value to represent the path to the users home directory.
! This avoids hard coding in a users name and thus makes these commands scriptable and instantly usable by others
!
! The other thing we have done is created an alias in ~/.shrc
! alias rxr='xrdb -load ~/.Xresources'
! Thus by running the command rxr we can reload our .Xresources file should we make any changes to it.
!
! In summary this is what our .xinitrc files contents look like to load and configure ~/.Xresources:
!
! xrdb -load $HOME/.Xresources
! xrdb -merge $HOME/.config/Xresources/themes/dark.nord


! ENABLING FONTS
! -----------------------------------------------------------------------------
!
! To install the X Window System, we have two choices:
!  i) xorg
! ii) xorg-minimal
!
! I have opted to go with xorg-minimal as it does not flood our system with fonts.
! pkg install xorg-minimal
!
! Next we need to install our video driver
! pkg install drm-kmod
! 
! But we will need to add a few packages that xorg-minimal does not include
! These packages are:
! xterm
! xfontsel
! xmessage
!
! We will also need to install the font inconsolata
! pkg install inconsolata 
!
! In the example above, we have tried to set the Inconsolata font for the xmessage application.
! We are using xmessage as this is the default application used by the Xmonad Window Manager to display keybindings.
! xmessage is one of those applcations that uses fonts configured in the old system.
! But before that, we have to let X know where our font paths are.
!
! The FreeBSD manual seems slightly less useful here.
! It tells us that the path to use is here: 
! /usr/local/etc/X11/xorg.conf.d
!
! What changes things is that many folks today are installing graphics using drm-kmod.
! By studying my own install carefully, I have deduced that the configuration file is located here: 
! /usr/local/share/X11/xorg.conf.d
! 
! To add fonts:
! Switch to root
! ee /usr/local/share/X11/xorg.conf.d/15-fonts.conf
!
! Populate file with:
! 
! Section "Files"
!     ModulePath "/usr/local/lib/xorg/modules"
!     FontPath   "/usr/local/share/fonts/dejavu"
!     FontPath   "/usr/local/share/fonts/inconsolata"
! EndSection
!
! KIV
! We noticed a message from the install logs for dejavu-2.37_1 that said:
! Add the following line to the "Modules" section of our x config file:
!     Load "freetype"
! I will need to Google this!
!
!

! REFERENCES
! -----------------------------------------------------------------------------
!
! http://invisible-island.net/xterm/xterm.faq.html
! https://raw.githubusercontent.com/vermaden/scripts/master/DOT.Xdefaults
! https://unixsheikh.com/tutorials/how-to-setup-freebsd-with-a-riced-desktop-part-3-i3.html#xterm



! NOTES
! -----------------------------------------------------------------------------
! The day may come where our Backspace and Delete keys do not work as expected in xterm.
! This should be expected when we find ourselves working in mixed environments.
! We have to deal with unix, linux, emacs, vim, ssh, x11, console

! What the 2 lines below do is tell xterm to exchange the Backspace and Delete keys with each other.
! https://tools.ietf.org/doc/xterm/xterm.faq.html
! <Key>Delete: string(0x1b) string("[3~") \n\
! <Key>BackSpace: string(0x7f) \n\

! By default xterm does not use the X11 clipboard
! The following line enables xterm to copy selections to the clipboard.
! xterm*selectToClipboard: true

! The following line enables xterm to add selection to both the primary buffer and clipboard.
! <Btn1Up>: select-end(PRIMARY, CLIPBOARD, CUT_BUFFER0)

